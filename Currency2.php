<?php

/**
 * Description of Currency2
 *
 * @author Mazhar
 */
class Currency2 {

    public function germany_to_new_zeland($amount) {
        $result = $amount * 1.52;
        echo '<b>' . $amount . '</b>' . ' EUR = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'NZ$';
    }

    public function germany_to_argentina($amount) {
        $result = $amount * 16.54;
        echo '<b>' . $amount . '</b>' . ' EUR = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'ARS';
    }

    public function germany_to_usa($amount) {
        $result = $amount * 1.09;
        echo '<b>' . $amount . '</b>' . ' EUR = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'US$';
    }

    public function germany_to_australia($amount) {
        $result = $amount * 1.42;
        echo '<b>' . $amount . '</b>' . ' EUR = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'A$';
    }

    public function argentina_to_new_zeland($amount) {
        $result = $amount * 0.092;
        echo '<b>' . $amount . '</b>' . ' ARS = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'NZ$';
    }

    public function argentina_to_germany($amount) {
        $result = $amount * 0.060;
        echo '<b>' . $amount . '</b>' . ' ARS = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'EUR';
    }

    public function argentina_to_usa($amount) {
        $result = $amount * 0.066;
        echo '<b>' . $amount . '</b>' . ' ARS = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'US$';
    }

    public function argentina_to_australia($amount) {
        $result = $amount * 0.086;
        echo '<b>' . $amount . '</b>' . ' ARS = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'A$';
    }

    public function usa_to_new_zeland($amount) {
        $result = $amount * 1.40;
        echo '<b>' . $amount . '</b>' . ' US$ = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'NZ$';
    }

    public function usa_to_germany($amount) {
        $result = $amount * 0.92;
        echo '<b>' . $amount . '</b>' . ' US$ = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'EUR';
    }

    public function usa_to_australia($amount) {
        $result = $amount * 1.31;
        echo '<b>' . $amount . '</b>' . ' US$ = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'A$';
    }

    public function usa_to_argentina($amount) {
        $result = $amount * 15.20;
        echo '<b>' . $amount . '</b>' . ' US$ = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'ARS';
    }

    public function australia_to_new_zeland($amount) {
        $result = $amount * 1.07;
        echo '<b>' . $amount . '</b>' . ' A$ = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'NZ$';
    }

    public function australia_to_germany($amount) {
        $result = $amount * 0.70;
        echo '<b>' . $amount . '</b>' . ' A$ = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'EUR';
    }

    public function australia_to_usa($amount) {
        $result = $amount * 0.76;
        echo '<b>' . $amount . '</b>' . ' A$ = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'US$';
    }

    public function australia_to_argentina($amount) {
        $result = $amount * 11.63;
        echo '<b>' . $amount . '</b>' . ' A$ = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'ARS';
    }

    public function new_zelanda_to_australia($amount) {
        $result = $amount * 0.94;
        echo '<b>' . $amount . '</b>' . ' NZ$ = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'A$';
    }

    public function new_zelanda_to_germany($amount) {
        $result = $amount * 0.66;
        echo '<b>' . $amount . '</b>' . ' NZ$ = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'EUR';
    }

    public function new_zelanda_to_usa($amount) {
        $result = $amount * 0.72;
        echo '<b>' . $amount . '</b>' . ' NZ$ = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'US$';
    }

    public function new_zelanda_to_argentina($amount) {
        $result = $amount * 10.91;
        echo '<b>' . $amount . '</b>' . ' NZ$ = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'ARS';
    }

}
