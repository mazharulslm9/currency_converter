<?php

/**
 * Description of Currency
 *
 * @author Mazhar
 */
class Currency {

    public function bangladeshi_tk_to_indian_rupee($amount) {
        $result = $amount * 0.85;
        echo '<b>' . $amount . '</b>' . ' BDT = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'INR';
    }

    public function bangladeshi_tk_to_pakistani_rupee($amount) {
        $result = $amount * 1.34;
        echo '<b>' . $amount . '</b>' . ' BDT = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'PKR';
    }

    public function bangladeshi_tk_to_sri_lankan_rupee($amount) {
        $result = $amount * 1.87;
        echo '<b>' . $amount . '</b>' . ' BDT = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'LKR';
    }

    public function bangladeshi_tk_to_japanese_yen($amount) {
        $result = $amount * 1.33;
        echo '<b>' . $amount . '</b>' . ' BDT = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'JPY';
    }

    public function bangladeshi_tk_to_chinese_yuan($amount) {
        $result = $amount * 0.086;
        echo '<b>' . $amount . '</b>' . ' BDT = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'CNY';
    }

    public function bangladeshi_tk_to_malaysian_ringgit($amount) {
        $result = $amount * 0.053;
        echo '<b>' . $amount . '</b>' . ' BDT = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'MYR';
    }

    public function bangladeshi_tk_to_maldivian_rufiyaa($amount) {
        $result = $amount * 0.20;
        echo '<b>' . $amount . '</b>' . ' BDT = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'MVR';
    }

    public function bangladeshi_tk_to_russian_ruble($amount) {
        $result = $amount * 0.79;
        echo '<b>' . $amount . '</b>' . ' BDT = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'RUB';
    }

    public function bangladeshi_tk_to_saudi_riyal($amount) {
        $result = $amount * 0.048;
        echo '<b>' . $amount . '</b>' . ' BDT = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'SAR';
    }
      public function bangladeshi_tk_to_singapore_dollar($amount) {
        $result = $amount * 0.018;
        echo '<b>' . $amount . '</b>' . ' BDT = ' . '<b>' . number_format((abs($result)), 2, '.', '') . '</b>' . ' ' . 'SGD';
    }

}
