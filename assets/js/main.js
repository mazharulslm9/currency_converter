/*========= Data refresh jquery =============*/
setTimeout(function () {
    $('#message').fadeOut('slow');
    $('#amnt').val('');
    $('#tags').val('');
    $('#message2').fadeOut('slow');
    $('#amnt2').val('');
}, 10000);

/*========= Autocomplete jquery =============*/
$(function () {
    var availableTags = [
        "India",
        "Pakistan",
        "Sri Lanka",
        "Japan",
        "China",
        "Malaysia",
        "Maldives",
        "Russia",
        "Saudi Arabia",
        "Singapore"
    ];
    $("#tags").autocomplete({
        source: availableTags
    });
});
