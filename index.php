
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Currency Converter</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/style1.css">
        <script src="assets/js/modernizr.custom.63321.js"></script>

    </head>
    <body>
        <section class="converter main clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <header><h3 class="text-center">Single Way Convertion</h3></header>
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title text-center">BDT TO ASIAN CURRENCY</h3>
                            </div>
                            <div class="panel-body">
                                <form class="form-group" action="" method="post">
                                    <label class="form-group">Enter Amount(Bangladeshi TK):</label>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-secondary" type="button"><img src="assets/images/tk.png" style="display: block;width: 16px;"></button>
                                        </span>
                                        <input id="amnt" value="<?php if (isset($_POST['amount'])) echo $_POST['amount']; ?>" type="text" name="amount" class="form-control" placeholder="Enter Amount(Bangladeshi TK)" required="">

                                    </div>
                                    <br>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-secondary" type="button">Country</button>
                                        </span>
                                        <input  value="<?php if (isset($_POST['country_name'])) echo $_POST['country_name']; ?>" class="form-control" name="country_name" placeholder="Type Country name" id="tags" required="">

                                    </div>
                                    <input class="form-control btn btn-info text-center" type="submit" value="Convert Amount" name="submit">
                                </form>

                                <div id="message" class="text-center">
                                    <?php require './result.php'; ?>

                                </div>

                            </div>

                        </div>

                    </div>
                    <div class=" col-md-8">
                        <header><h3 class="text-center">Double Way Convertion</h3></header>
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title text-center">CURRENCY CONVERTER</h3>
                            </div>
                            <div class="panel-body">
                                <form class="form-group" action="" method="post">
                                    <label class="form-group">Enter Amount:</label>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-secondary" type="button">Currency Amount</button>
                                        </span>
                                        <input id="amnt2" value="<?php if (isset($_POST['amount2'])) echo $_POST['amount2']; ?>" type="text" name="amount2" class="form-control" placeholder="Enter Amount" required="">

                                    </div>
                                    <br>
                                    <div class="input-group">
                                        <div class="col-md-6">
                                            <div class="fleft">
                                                <select  id="cd-dropdown" name="country1" class="cd-select">
                                                    <option selected="" value="germany">Germany</option>
                                                    <option value="argentina">Argentina</option>
                                                    <option value="usa">USA</option>
                                                    <option value="australia">Australia</option>
                                                    <option value="new">New Zeland</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="fleft2">
                                                <select id="cd-dropdown2" name="country2" class="cd-select">
                                                    <option selected="" value="new">New Zeland</option>
                                                    <option value="germany">Germany</option>
                                                    <option value="argentina">Argentina</option>
                                                    <option value="usa">USA</option>
                                                    <option value="australia">Australia</option>

                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                    <input class="form-control btn btn-info text-center" type="submit" value="Convert Amount" name="right">
                                </form>
                                <div id="message2" class="text-center">
                                    <?php require './result2.php'; ?>

                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>
        </section>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/jquery.dropdown.js"></script>

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script type="text/javascript">

            $(function () {

                $('#cd-dropdown').dropdown({
                    gutter: 0
                });
                $('#cd-dropdown2').dropdown({
                    gutter: 0
                });

            });

        </script>




    </body>
</html>

